#ifndef ENTRANCE_CONF_USER_H_
#define ENTRANCE_CONF_USER_H_

void entrance_conf_user_init(void);
void entrance_conf_user_shutdown(void);

#endif /* ENTRANCE_CONF_USER_H_ */
